package com.verbund.vienna.verbund;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.verbund.vienna.verbund.Data.Meter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.ValueShape;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.LineChartView;

/**
 * Created by Maj-PC on 2. 02. 2018.
 */

public class MeterCard extends RecyclerView.ViewHolder {
    @Bind(R.id.thing_chart)
    LineChartView thing_chart;
    @Bind(R.id.tv_card_meter_name)
    TextView tv_card_meter_name;
    @Bind(R.id.tv_card_last_update)
    TextView tv_card_last_update;
    @Bind(R.id.tv_card_current_power)
    TextView tv_card_current_power;
    @Bind(R.id.iv_settings)
    ImageView iv_settings;

    private Context context;
    private Meter meter;

    public MeterCard(View view, Context context){
        super(view);
        ButterKnife.bind(this, view);
        this.context = context;
    }
    public void bind(Meter meter) {
        this.meter = meter;
        tv_card_meter_name.setText(meter.getMeterName());
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm  dd/MM", Locale.getDefault());
        tv_card_last_update.setText(sdf.format(meter.getLastUpdate()));
        tv_card_current_power.setText(String.valueOf(meter.getCurrentPower()));

        iv_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = (new Intent(context, ConfigurationActivity.class));
//                i.putExtra(ConfigurationActivity.METER_ID, meter.getMeterName());
                context.startActivity(i);
            }
        });
        generateValues();
        generateData();
    }

    private int numberOfLines = 1;
    private int numberOfPoints = 60;
    private LineChartData data;
    float[][] randomNumbersTab = new float[numberOfLines][numberOfPoints];
    private void generateData() {

        List<Line> lines = new ArrayList<Line>();
        for (int i = 0; i < numberOfLines; ++i) {

            List<PointValue> values = new ArrayList<PointValue>();
            for (int j = 0; j < numberOfPoints; ++j) {
                values.add(new PointValue(j, randomNumbersTab[i][j]));
            }

            Line line = new Line(values);
            setLineStyleToSensor(line, ContextCompat.getColor(context, R.color.colorPrimary));
            lines.add(line);
        }

        data = new LineChartData(lines);

        if (true) {
            Axis axisX = new Axis();
            Axis axisY = new Axis().setHasLines(true);
            if (true) {
                axisX.setName("Time");
                axisY.setName("Power (W)");
            }
            data.setAxisXBottom(axisX);
            data.setAxisYLeft(axisY);
        }

        data.setBaseValue(Float.NEGATIVE_INFINITY);
        thing_chart.setLineChartData(data);
    }
    private void generateValues() {
        for (int i = 0; i < numberOfLines; ++i) {
            for (int j = 0; j < numberOfPoints; ++j) {
                randomNumbersTab[i][j] = (float) Math.random() * 70f;
            }
        }
    }
    public static void setLineStyleToSensor(Line line, int color) {
        line.setColor(color);
        line.setShape(ValueShape.CIRCLE);
        line.setPointRadius(1);
        line.setCubic(false);
        line.setFilled(true);
        line.setAreaTransparency(20);
        line.setHasLabels(false);
        line.setHasLabelsOnlyForSelected(false);
        line.setHasLines(true);
        line.setHasPoints(false);
        line.setStrokeWidth(1);
    }
}
