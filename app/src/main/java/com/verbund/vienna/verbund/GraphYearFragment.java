package com.verbund.vienna.verbund;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.SubcolumnValue;
import lecho.lib.hellocharts.model.ValueShape;
import lecho.lib.hellocharts.view.ColumnChartView;
import lecho.lib.hellocharts.view.LineChartView;

/**
 * Created by Maj-PC on 6. 02. 2018.
 */

public class GraphYearFragment extends Fragment {
    @Bind(R.id.line_chart)
    LineChartView line_chart;
    @Bind(R.id.column_chart)
    ColumnChartView column_chart;
    @Bind(R.id.tv_card_meter_name)
    TextView tv_card_meter_name;
    @Bind(R.id.sp_dropdown)
    Spinner sp_dropdown;
    String[] dropDownItems = new String[]{"First meter 1, Second meter 2"};

    @Bind(R.id.tv_date)
    TextView tv_date;
    @Bind(R.id.btn_graph_day)
    Button btn_graph_day;
    @Bind(R.id.btn_graph_week)
    Button btn_graph_week;
    @Bind(R.id.btn_graph_month)
    Button btn_graph_month;
    @Bind(R.id.btn_graph_year)
    Button btn_graph_year;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_graph_base, container, false);
        ButterKnife.bind(this, view);

        // Inflate the layout for this fragment
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, dropDownItems);
//set the spinners adapter to the previously created one.
        sp_dropdown.setAdapter(adapter);
        sp_dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tv_card_meter_name.setText(dropDownItems[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        generateDefaultData();
        line_chart.setVisibility(View.GONE);
        column_chart.setVisibility(View.VISIBLE);
        return view;
    }

    private void generateDefaultData() {
        int numSubcolumns = 1;
        int numColumns = 12;
        // Column can have many subcolumns, here by default I use 1 subcolumn in each of 8 columns.
        List<Column> columns = new ArrayList<Column>();
        List<SubcolumnValue> values;
        for (int i = 0; i < numColumns; ++i) {

            values = new ArrayList<SubcolumnValue>();
            for (int j = 0; j < numSubcolumns; ++j) {
                values.add(new SubcolumnValue((float) Math.random() * 50f + 5, ContextCompat.getColor(getContext(), R.color.colorSecondary)));
            }

            Column column = new Column(values);
//            column.setHasLabels(true);
            columns.add(column);
        }

        ColumnChartData data = new ColumnChartData(columns);

        Axis axisX = new Axis();
        Axis axisY = new Axis().setHasLines(true);
        axisX.setName("Month in year");
        axisY.setName("Energy [kWh]");
        data.setAxisXBottom(axisX);
        data.setAxisYLeft(axisY);
        column_chart.setColumnChartData(data);
    }
}
