package com.verbund.vienna.verbund;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.SubcolumnValue;
import lecho.lib.hellocharts.model.ValueShape;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.ColumnChartView;
import lecho.lib.hellocharts.view.LineChartView;

/**
 * Created by Maj-PC on 4. 02. 2018.
 */

public class DiagramsFragment extends Fragment {
    @Bind(R.id.line_chart)
    LineChartView line_chart;
    @Bind(R.id.column_chart)
    ColumnChartView column_chart;
    @Bind(R.id.tv_card_meter_name)
    TextView tv_card_meter_name;
    @Bind(R.id.sp_dropdown)
    Spinner sp_dropdown;
    String[] dropDownItems = new String[]{"First meter 1", "Second meter 2"};

    @Bind(R.id.tv_date)
    TextView tv_date;
    @Bind(R.id.btn_graph_day)
    Button btn_graph_day;
    @Bind(R.id.btn_graph_week)
    Button btn_graph_week;
    @Bind(R.id.btn_graph_month)
    Button btn_graph_month;
    @Bind(R.id.btn_graph_year)
    Button btn_graph_year;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setHasOptionsMenu(true);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_graph_base, container, false);
        ButterKnife.bind(this, view);

        // Inflate the layout for this fragment
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, dropDownItems);
//set the spinners adapter to the previously created one.
        sp_dropdown.setAdapter(adapter);
        sp_dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tv_card_meter_name.setText(dropDownItems[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btn_graph_day.setOnClickListener(timeSpanClickListener);
        btn_graph_week.setOnClickListener(timeSpanClickListener);
        btn_graph_month.setOnClickListener(timeSpanClickListener);
        btn_graph_year.setOnClickListener(timeSpanClickListener);
        return view;
    }

    private View.OnClickListener timeSpanClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Button btn = (Button) v;
            if (btn.equals(btn_graph_day)) {
                setButtonSelected(btn_graph_day, true);
                setButtonSelected(btn_graph_week, false);
                setButtonSelected(btn_graph_month, false);
                setButtonSelected(btn_graph_year, false);
                generateGraphData(true, 0);
                tv_date.setText("7.1.2018");
            }
            if (btn.equals(btn_graph_week)) {
                setButtonSelected(btn_graph_day, false);
                setButtonSelected(btn_graph_week, true);
                setButtonSelected(btn_graph_month, false);
                setButtonSelected(btn_graph_year, false);
                generateGraphData(false, 8);
                tv_date.setText("7.1.2018 - 14.1.2018");
            }
            if (btn.equals(btn_graph_month)) {
                setButtonSelected(btn_graph_day, false);
                setButtonSelected(btn_graph_week, false);
                setButtonSelected(btn_graph_month, true);
                setButtonSelected(btn_graph_year, false);
                generateGraphData(false, 30);
                tv_date.setText("January 2018");
            }
            if (btn.equals(btn_graph_year)) {
                setButtonSelected(btn_graph_day, false);
                setButtonSelected(btn_graph_week, false);
                setButtonSelected(btn_graph_month, false);
                setButtonSelected(btn_graph_year, true);
                generateGraphData(false, 12);
                tv_date.setText("2018");
            }
        }
    };
    private void setButtonSelected(Button btn, boolean selected) {
        btn.setBackground(selected ? getResources().getDrawable(R.color.colorPrimary) : getResources().getDrawable(R.drawable.border_rectangle_color));
        btn.setTextColor(selected ? ContextCompat.getColor(getContext(), R.color.white) : ContextCompat.getColor(getContext(), R.color.black));
//        btn.setBackground(getResources().getDrawable(R.color.colorPrimary));
    }
//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        // TODO Add your menu entries here
//        menu.clear();
//        inflater.inflate(R.menu.menu_base, menu);
//        super.onCreateOptionsMenu(menu, inflater);
//    }
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        return true;
//    }

    private void generateGraphData(boolean isLine, int numColumns) {
        if (!isLine) {
            generateDefaultData(numColumns);
            line_chart.setVisibility(View.GONE);
            column_chart.setVisibility(View.VISIBLE);
        } else {
            column_chart.setVisibility(View.GONE);
            line_chart.setVisibility(View.VISIBLE);
            generateValues();
            generateData();
        }
    }

    private void generateDefaultData(int numColumns) {
        int numSubcolumns = 1;
        // Column can have many subcolumns, here by default I use 1 subcolumn in each of 8 columns.
        List<Column> columns = new ArrayList<Column>();
        List<SubcolumnValue> values;
        for (int i = 0; i < numColumns; ++i) {

            values = new ArrayList<SubcolumnValue>();
            for (int j = 0; j < numSubcolumns; ++j) {
                values.add(new SubcolumnValue((float) Math.random() * 50f + 5, ContextCompat.getColor(getContext(), R.color.colorSecondary)));
            }

            Column column = new Column(values);
//            column.setHasLabels(true);
            columns.add(column);
        }

        ColumnChartData data = new ColumnChartData(columns);

        Axis axisX = new Axis();
        Axis axisY = new Axis().setHasLines(true);
        axisX.setName(numColumns == 12 ? "Month in year" : numColumns == 30 ? "Day in month" : "Day in week");
        axisY.setName("Energy [kWh]");

        data.setAxisXBottom(axisX);
        data.setAxisYLeft(axisY);

        column_chart.setColumnChartData(data);
    }

    private int numberOfLines = 1;
    private int numberOfPoints = 24 * 10;
    private LineChartData data;
    float[][] randomNumbersTab = new float[numberOfLines][numberOfPoints];

    private void generateData() {

        List<Line> lines = new ArrayList<Line>();
        for (int i = 0; i < numberOfLines; ++i) {

            List<PointValue> values = new ArrayList<PointValue>();
            for (int j = 0; j < numberOfPoints; ++j) {
                values.add(new PointValue(j, randomNumbersTab[i][j]));
            }

            Line line = new Line(values);
            setLineStyleToSensor(line, ContextCompat.getColor(getContext(), R.color.colorSecondary));
            lines.add(line);
        }

        data = new LineChartData(lines);

        Axis axisX = new Axis();
        Axis axisY = new Axis().setHasLines(true);
        axisX.setName("Time");
        axisY.setName("Power (W)");
        data.setAxisXBottom(axisX);
        data.setAxisYLeft(axisY);

        data.setBaseValue(Float.NEGATIVE_INFINITY);
        line_chart.setLineChartData(data);
    }

    private void generateValues() {
        for (int i = 0; i < numberOfLines; ++i) {
            for (int j = 0; j < numberOfPoints; ++j) {
                randomNumbersTab[i][j] = (float) Math.random() * 5f + (float) Math.sin(j * 2 * Math.PI / numberOfPoints) * 35f + 35f;
            }
        }
    }

    public static void setLineStyleToSensor(Line line, int color) {
        line.setColor(color);
        line.setShape(ValueShape.CIRCLE);
        line.setPointRadius(1);
        line.setCubic(false);
        line.setFilled(true);
        line.setAreaTransparency(20);
        line.setHasLabels(false);
        line.setHasLabelsOnlyForSelected(false);
        line.setHasLines(true);
        line.setHasPoints(false);
        line.setStrokeWidth(1);
    }
}
