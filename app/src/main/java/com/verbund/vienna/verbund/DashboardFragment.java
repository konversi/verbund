package com.verbund.vienna.verbund;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.github.clans.fab.FloatingActionButton;
import com.verbund.vienna.verbund.Adapter.MeterAdapter;
import com.verbund.vienna.verbund.Data.Meter;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Maj-PC on 2. 02. 2018.
 */

public class DashboardFragment extends Fragment {

    @Bind(R.id.rv_things)
    RecyclerView rv_things;

    private MeterAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ButterKnife.bind(this, view);

        List<Meter> meterList = new ArrayList<>();
        meterList.add(new Meter("First meter 1", System.currentTimeMillis(), 132));
        meterList.add(new Meter("Second meter 2", System.currentTimeMillis()/2, 3715));
        adapter = new MeterAdapter(getContext(), meterList);
        rv_things.setAdapter(adapter);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        rv_things.setLayoutManager(manager);

        // Inflate the layout for this fragment
        return view;
    }
}
