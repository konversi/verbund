package com.verbund.vienna.verbund.Data;

/**
 * Created by Maj-PC on 3. 02. 2018.
 */

public class Meter {
    public static final String KEY_UUID = "id";
//    @PrimaryKey // Message id cannot be null
//    private String id;

    private String meterName = "";
    private long lastUpdate = 0L;
    private int currentPower = 0;

    public Meter(String name, long update, int power) {
        meterName = name;
        lastUpdate = update;
        currentPower = power;
    }

    public void setMeterName(String name) { this.meterName = name;}
    public String getMeterName() { return meterName;}

    public void setLastUpdate(long lastUpdate) { this.lastUpdate = lastUpdate;}
    public long getLastUpdate() { return lastUpdate;}

    public void setCurrentPower(int name) { this.currentPower = currentPower;}
    public int getCurrentPower() { return currentPower;}
}
