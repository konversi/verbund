package com.verbund.vienna.verbund.Adapter;

/**
 * Created by Maj-PC on 3. 02. 2018.
 */

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.verbund.vienna.verbund.DashboardFragment;
import com.verbund.vienna.verbund.DiagramsFragment;

public class MainPagerAdapter  extends FragmentPagerAdapter {

    SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

    final int PAGE_COUNT = 2;
    private String tabTitles[] = new String[] {
            "Dashboard",
            "Diagrams",
    };
    public boolean tabIndicators[] = new boolean[] {
            false, false
    };

    public MainPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
//        tabTitles = new String[] {
//                context.getString(R.string.dashboard),
//                context.getString(R.string.appliances),
//        };
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                //return DashboardFragment.newInstance();
                return new DashboardFragment();
            case 1:
                return new DiagramsFragment();
            default:
                return new Fragment();
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position] + ((tabIndicators[position]) ? " *" : "");
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }
}