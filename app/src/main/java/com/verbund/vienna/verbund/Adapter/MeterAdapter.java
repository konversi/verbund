package com.verbund.vienna.verbund.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.verbund.vienna.verbund.Data.Meter;
import com.verbund.vienna.verbund.MeterCard;
import com.verbund.vienna.verbund.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Maj-PC on 3. 02. 2018.
 */

public class MeterAdapter extends RecyclerView.Adapter<MeterCard> {

    private List<Meter> meterList = new ArrayList<>();
    private Context context;

    public MeterAdapter(Context context, List<Meter> meters) {
        this.context = context;
        this.meterList = meters;
    }

    @Override
    public void onBindViewHolder(MeterCard meterCard, int i) {
        meterCard.bind(meterList.get(i));
    }

    @Override
    public MeterCard onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView;
        itemView = LayoutInflater.from(context).inflate(R.layout.cardview_meter, viewGroup, false);
        return new MeterCard(itemView, context);
    }

    @Override
    public int getItemCount() {
        return meterList.size();
    }
}
